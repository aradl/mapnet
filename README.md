# MapNET

Gather trace and latency information about websites

## Features

 * TBD

 ### Prerequisites

 ```
 python3
 ```

 ### Running and Setup 

 To execute the script 

 ```
 mkdir ; cd ;
 wget https://gitlab.com/aradl/SOMETHING;
 mkdir ;
 ```
 To Run:

 ```
 ./python3 ./mapnet.py
 ```

 Output Example:

 ```
 ...etc 
 ```

 ## Built With

  * [Python3](http://www.python.org) - Language

  ## Authors

   * **Andrew** - [aradl](https://gitlab.com/aradl)


