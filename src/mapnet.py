#!/usr/bin/env python3

import os
import sys
import subprocess
import argparse
import datetime

import json
import csv
from urllib.request import urlopen

import numpy as np
import matplotlib.pyplot as plt

STAT_AMT      = '100'
PING_CMD      = 'ping -c 2 '
PING_STAT_CMD = 'ping -c ' + STAT_AMT + ' '
PING_BLK_CMD  = 'sudo nping --tcp -c 2 '
PING_STAT_BLK_CMD  = 'sudo nping --tcp -c ' + STAT_AMT + ' '
PING_AWK      = ''#" | grep rtt | awk '{print $4}'"

TRACE_CMD = 'traceroute '
TRACE_AWK = ' | grep -v traceroute '#" | awk '{print $4}'"

IPINFO_TOKEN_FILE = 'ipinfo_token'

URL_IP     = 'http://ipinfo.io/'
URL_IP_TOK = '/geo?token='

# parse input file
def import_file(map_file): 
    inlist = []
    
    with open(map_file) as file:
        for i, line in enumerate(file):
            # remove new line, commas and store string in list
            line = line.strip()
            inlist.append(line)

    return inlist


class URL_info():
    def __init__(self, url, token, args):
        # setup
        self.url   = url
        self.token = token

        # get ip and delay
        self.ip, self.delay = self.get_ping_info(self.url)

        # get location
        self.location = self.get_ip_location(self.ip)

        if(args.trace):
            # get trace information
            self.trace = self.get_trace_info(self.url)

    # return ip address and ping delay
    def get_ping_info(self, url):
        ip        = ''
        delay     = ''
    
        # run command
        try:
            output = subprocess.check_output(PING_CMD + url + PING_AWK, shell=True,stderr=subprocess.STDOUT)
            dstr   = output.decode("ascii").split("\n")
        except subprocess.CalledProcessError as e:
            dstr   = e.output.decode("ascii").split("\n")

            # normal ICMP is blocked, lets use tcp to ping
            delay = self.tcp_ping(url)

        # parse output
        for line in dstr:
            if("PING" in line):
                ip = line[line.find("(")+1:line.find(")")]
            if(("rtt" in line) and (not delay)):
                delay = line.split("/")[5]
    
        return ip, delay

    # return ip address and ping delay
    def tcp_ping(self, url):
        ip        = ''
        delay     = ''

        # issue warning
        print('INFO: ' + url + ' is blocking ICMP (using nping --tcp)')
    
        # run command
        output = subprocess.check_output(PING_BLK_CMD + url + PING_AWK, shell=True)
        dstr   = output.decode("ascii").split("\n")
    
        # parse output
        for line in dstr:
            if("rtt" in line):
                delay = line.split("|")[2][9:-2]
    
        return delay
        
    # return ip address and ping delay
    def get_ping_stat(self):
        inv_delay = []
    
        # run command
        try:
            output = subprocess.check_output(PING_STAT_CMD + self.url + PING_AWK, shell=True,stderr=subprocess.STDOUT)
            dstr   = output.decode("ascii").split("\n")

            # parse output
            for line in dstr:
                if("bytes from" in line):
                    inv_delay.append(line[line.find("time=")+5:-2])

        # normal ICMP is blocked, lets use tcp to ping
        except subprocess.CalledProcessError as e:
            # issue warning
            print('INFO: ' + self.url + ' is blocking ICMP (using nping --tcp)')

            output = subprocess.check_output(PING_STAT_BLK_CMD + self.url + PING_AWK, shell=True,stderr=subprocess.STDOUT)
            dstr   = output.decode("ascii").split("\n")

            sent_time = []
            rcvd_time = []

            # parse output
            for line in dstr:
                if("SENT" in line):
                    sent_time.append(float(line[line.find("(")+1:line.find(")")-1]))
                if("RCVD" in line):
                    rcvd_time.append(float(line[line.find("(")+1:line.find(")")-1]))

            # preform delay math
            rcvd      = np.array(rcvd_time)
            sent      = np.array(sent_time)
            sent.resize(rcvd.shape)
            delays    = rcvd - sent
            delays    = np.abs(delays) * 1000 # seconds to ms
            inv_delay = delays.tolist()
    
        return inv_delay
    
    # input ip address and return long and lat
    def get_ip_location(self, ip):
        # call API
        if("192.168" in ip):
            # only I know this
            location = '42.3722,-71.1787'
        else:
            response = urlopen(URL_IP + ip + URL_IP_TOK + self.token)

            # parse information
            data     = json.load(response)
            location = data['loc']
    
        return location

    def get_trace_info(self, url):
        output = subprocess.check_output(TRACE_CMD + url + TRACE_AWK, shell=True)
        dstr   = output.decode("ascii").split("\n")

        # parse output
        trace = []
        for line in dstr:
            ip    = line[line.find("(")+1:line.find(")")]
            delay = line[line.find(")")+3:line.find(")")+9]
    
            if(not ('*' in ip) and ip):
                loc = self.get_ip_location(ip)

                # build inner list
                trace.append([ip, delay, loc])

        return trace

    def output_trace(self):
        # Write CSV file
        with open('trace_' + self.url + '.csv', 'w') as fp:
            fp.write('lat,lng,comment' + '\n')
            for part in self.trace:
                fp.write(part[2] + ', ' + part[0] + ' ( ' + part[1] + 'ms)' + '\n')
        
    def output_ping_stats(self, delay_list):
        # Write CSV file
        isotime = datetime.datetime.now().replace(microsecond=0).isoformat()
        with open('ping_' + self.url + '_' + isotime + '.csv', 'w') as fp:
            for part in delay_list:
                if(isinstance(part, float)):
                    fp.write('%.3f' % part + '\n')
                else:
                    fp.write(part + '\n')

    def print_info(self):
        print("URL: {3}, IP: {0}, t_ping: {1}, location {2}".format(self.ip, self.delay, self.location, self.url))

    def plot_trace(self):
        print("")
        

def main():
    '''
    Entrance of the program
    '''

    # parse command line arguments
    parser = argparse.ArgumentParser()
    # arguments
    parser.add_argument("file", type=str, help='Input file for URL list to gather data on', 
                        nargs='?', const=1, default='url.list')
    parser.add_argument('-s', '--stats', help='Generate stats file')
    parser.add_argument('-a', '--ana', help='Create plots from ping information')
    parser.add_argument('-t', '--trace', help='Gather trace info')
    args = parser.parse_args()

    # read input file and create a list of URLs
    url_list = import_file(args.file)
    # read in token information
    ipinfo_token = import_file(IPINFO_TOKEN_FILE)[0]

    # loop through and get ping results
    # [ [url, ip, delay, [long, lat]], [...]]
    url_info = []
    for url in url_list:
        info = URL_info(url, ipinfo_token, args)
        info.print_info()

        # build trace file
        if(args.trace):
            print('building trace ...')
            info.output_trace()
            
        # build list
        url_info.append(info) 

    if(args.stats):
        # run and output ping stats
        for web in url_info:
            print('gathering stats for: ' + web.url + ' ...')
            sta = web.get_ping_stat()
            web.output_ping_stats(sta)

    if(args.ana):
        # analysis
        nisotime = datetime.datetime.now().replace(microsecond=0).isoformat()
        hist_foldername = 'hist_' + nisotime
        arch_foldername = hist_foldername + '/' + '_archive'

        # make some new folders to keep things organized
        if(not os.path.exists(hist_foldername)):
            os.makedirs(hist_foldername)

        if(not os.path.exists(arch_foldername)):
            os.makedirs(arch_foldername)

        np.set_printoptions(precision=3)
        for filename in os.listdir('.'):
            if filename.endswith(".csv") and ('ping' in filename):
                isotime = filename.split("_")[2][0:-4]
                url     = filename.split("_")[1]

                # grab data from file
                data = np.genfromtxt(filename, delimiter='\n')
                data_mean = '%.3f' % np.mean(data)
                data_min  = '%.3f' % np.amin(data)
                data_max  = '%.3f' % np.amax(data)
                info =  'min/avg/max = ' + data_min + '/' + data_mean + '/' + data_max + ' ms'
                
                plt.hist(data, bins='auto')
                plt.title('Histogram: ' + url + ' @ ' + isotime + '\n' + info)
                plt.xlabel('Ping Delay (ms)')
                plt.ylabel('Amount')

                # save histograms as an image
                plt.savefig(hist_foldername + '/' + 'hist' + filename[4:-4] + '.png')   
                plt.clf()

                # move all used csv files to archive
                os.rename(filename, arch_foldername + '/' + filename)

 
if __name__ == '__main__':
    main()
